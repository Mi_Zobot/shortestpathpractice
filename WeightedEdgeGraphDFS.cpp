#include "include/CommonStruct.h"

#include <climits>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void dfs(int &cur_path_cost, int &min_cost,
         UndirectedGraphNode* cur_node,
         UndirectedGraphNode* target_node,
         unordered_set<UndirectedGraphNode*> &visited)
 {
    if (cur_node == target_node) {
        min_cost = cur_path_cost < min_cost ? cur_path_cost : min_cost;
        return;
    }

    for (auto const &nb : cur_node->neighbors) {
        if (visited.find(nb.first) != visited.end()) {
            continue; // skip visited node
        }
        cur_path_cost += nb.second;
        visited.insert(nb.first);

        dfs(cur_path_cost, min_cost, nb.first, target_node, visited);

        visited.erase(nb.first);
        cur_path_cost -= nb.second;
    }
}

int FindShortestPathDFS(UndirectedGraphNode* start, UndirectedGraphNode* end) {
    // find all possible path from start to end then find min cost
    unordered_set<UndirectedGraphNode*> visited;
    visited.insert(start);
    int cur_path_cost = 0;
    int min_cost = INT_MAX;
    dfs(cur_path_cost, min_cost, start, end, visited);
    return min_cost;
}


void FindShortestPathToOtherVerticesDFS(UndirectedGraphNode* start) {
    unordered_set<UndirectedGraphNode*> all_nodes;

    queue<UndirectedGraphNode*> q;
    q.push(start);

    while(!q.empty()) {
        UndirectedGraphNode* cur_node = q.front();
        q.pop();
        if (all_nodes.find(cur_node) == all_nodes.end()) {
            all_nodes.insert(cur_node);
        }
        for (auto const &nb : cur_node->neighbors) {
            if (all_nodes.find(nb.first) == all_nodes.end()) {
                all_nodes.insert(nb.first);
                q.push(nb.first);
            }
        }
    }

    for (auto const &node : all_nodes) {
        if (node == start) {
            continue;
        }
        int lowest_cost = FindShortestPathDFS(start, node);
        cout << "cost from " << start->label << " to " << node->label << " is " << lowest_cost << endl;
    }
}


int main() {
    UndirectedGraphNode a(0);
    UndirectedGraphNode b(1);
    UndirectedGraphNode c(2);
    UndirectedGraphNode d(3);
    UndirectedGraphNode e(4);
    UndirectedGraphNode f(5);

    UndirectedGraphNode::AddNeighbors(&a, &b, 1);
    UndirectedGraphNode::AddNeighbors(&a, &c, 20);
    UndirectedGraphNode::AddNeighbors(&b, &d, 5);
    UndirectedGraphNode::AddNeighbors(&c, &d, 3);
    UndirectedGraphNode::AddNeighbors(&c, &e, 2);
    UndirectedGraphNode::AddNeighbors(&c, &f, 4);
    UndirectedGraphNode::AddNeighbors(&e, &f, 2);

    FindShortestPathToOtherVerticesDFS(&a);

    return 0;
}