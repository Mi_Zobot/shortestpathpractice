#include "include/CommonStruct.h"

#include <climits>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

typedef pair<int, UndirectedGraphNode*> CostNodePair;
typedef priority_queue<CostNodePair, vector<CostNodePair>, greater<CostNodePair>> CostNodePairPriorityQueue;

int UniformCostSearch(UndirectedGraphNode* start, UndirectedGraphNode* end) {
    // find all possible path from start to end then find min cost
    CostNodePairPriorityQueue frontier;
    unordered_set<UndirectedGraphNode*> explored;

    frontier.emplace(0, start);
    explored.insert(start);

    while (!frontier.empty()) {
        CostNodePair cur_cost_node = frontier.top();
        frontier.pop();

        if (cur_cost_node.second == end) {
            return cur_cost_node.first;
        }

        for (auto const &nb : cur_cost_node.second->neighbors) {
            int neighbor_cost = cur_cost_node.first + nb.second;
            if (explored.find(nb.first) == explored.end()) {
                explored.insert(nb.first);
                frontier.emplace(neighbor_cost, nb.first);
                continue;
            }
            // update frontier cost value if it's been explored
            // In theory we should modify the frontier cost insead of adding one.
            // This will consume extra space for the priority_queue
            frontier.emplace(neighbor_cost, nb.first);
        }
    }
    return -1;  //no path
}

void FindShortestPathToOtherVerticesUCS(UndirectedGraphNode* start) {
    unordered_set<UndirectedGraphNode*> all_nodes;

    // Step 1: find all nodes in the graph
    queue<UndirectedGraphNode*> q;
    q.push(start);
    while(!q.empty()) {
        UndirectedGraphNode* cur_node = q.front();
        q.pop();
        if (all_nodes.find(cur_node) == all_nodes.end()) {
            all_nodes.insert(cur_node);
        }
        for (auto const &nb : cur_node->neighbors) {
            if (all_nodes.find(nb.first) == all_nodes.end()) {
                all_nodes.insert(nb.first);
                q.push(nb.first);
            }
        }
    }

    // Step 2: find shortest path using UCS
    for (auto const &node : all_nodes) {
        if (node == start) {
            continue;
        }
        int lowest_cost = UniformCostSearch(start, node);
        cout << "cost from " << start->label << " to " << node->label << " is " << lowest_cost << endl;
    }
}


int main() {
    UndirectedGraphNode a(0);
    UndirectedGraphNode b(1);
    UndirectedGraphNode c(2);
    UndirectedGraphNode d(3);
    UndirectedGraphNode e(4);
    UndirectedGraphNode f(5);

    UndirectedGraphNode::AddNeighbors(&a, &b, 1);
    UndirectedGraphNode::AddNeighbors(&a, &c, 20);
    UndirectedGraphNode::AddNeighbors(&b, &d, 5);
    UndirectedGraphNode::AddNeighbors(&c, &d, 3);
    UndirectedGraphNode::AddNeighbors(&c, &e, 2);
    UndirectedGraphNode::AddNeighbors(&c, &f, 4);
    UndirectedGraphNode::AddNeighbors(&e, &f, 2);

    FindShortestPathToOtherVerticesUCS(&a);

    return 0;
}