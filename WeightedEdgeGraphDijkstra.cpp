#include "include/CommonStruct.h"

#include <climits>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;

UndirectedGraphNode* FindMinNode(unordered_map<UndirectedGraphNode*, int> const &vertex_set) {
    int cur_min = INT_MAX;
    UndirectedGraphNode* cur_node = nullptr;

    for (auto const &data : vertex_set) {
        if (data.second < cur_min) {
            cur_min = data.second;
            cur_node = data.first;
        }
    }

    return cur_node;
}

void InitDistPrev(UndirectedGraphNode* const start,
                  unordered_map<UndirectedGraphNode*, int> &dist,
                  unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> &prev)
{
    queue<UndirectedGraphNode*> q;
    q.push(start);

    while (!q.empty()) {
        UndirectedGraphNode* cur_node = q.front();
        q.pop();
        if (dist.find(cur_node) == dist.end()) {
            dist[cur_node] = INT_MAX;
            prev[cur_node] = nullptr;
        }
        for (auto nb : cur_node->neighbors) {
            if (dist.find(nb.first) == dist.end()) {
                q.push(nb.first);
                dist[nb.first] = INT_MAX;
                prev[nb.first] = nullptr;
            }
        }
    }

    dist[start] = 0;
}

void FindShortestPathToOtherVerticesDijkstra(UndirectedGraphNode* start) {
    // Step 1: Initialization
    unordered_map<UndirectedGraphNode*, int> dist;
    unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> prev;  // used for get start - end usage
    InitDistPrev(start, dist, prev);

    unordered_map<UndirectedGraphNode*, int> vertex_set;
    for (auto const &d : dist) {
        // d.first is UndirectedGraphNode*, d.second is the cost
        vertex_set[d.first] = d.second;
    }

    // Step 2: iterate throw all vertex
    while (!vertex_set.empty()) {
        UndirectedGraphNode* cur_node = FindMinNode(vertex_set);
        vertex_set.erase(cur_node);  // if we only need start - target then break.
        for (auto nb : cur_node->neighbors) {
            int alt = dist[cur_node] + nb.second;
            if (alt < dist[nb.first]) {
                dist[nb.first] = alt;
                prev[nb.first] = cur_node;
                vertex_set[nb.first] = alt;
            }
        }
    }

    // Step 3: print
    for (auto const &data : dist) {
        cout << "cost from " << start->label << " to " << data.first->label << " is: " << dist[data.first] << endl;
    }
}


int main() {
    UndirectedGraphNode a(0);
    UndirectedGraphNode b(1);
    UndirectedGraphNode c(2);
    UndirectedGraphNode d(3);
    UndirectedGraphNode e(4);
    UndirectedGraphNode f(5);

    UndirectedGraphNode::AddNeighbors(&a, &b, 1);
    UndirectedGraphNode::AddNeighbors(&a, &c, 20);
    UndirectedGraphNode::AddNeighbors(&b, &d, 5);
    UndirectedGraphNode::AddNeighbors(&c, &d, 3);
    UndirectedGraphNode::AddNeighbors(&c, &e, 2);
    UndirectedGraphNode::AddNeighbors(&c, &f, 4);
    UndirectedGraphNode::AddNeighbors(&e, &f, 2);

    FindShortestPathToOtherVerticesDijkstra(&a);

    return 0;
}