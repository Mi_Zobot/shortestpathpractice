# Shortest Path Practice

### build
```commandline
mkdir build && cd build
cmake ..
make
```

#### Undirected Graph with Weighted Edge (No neigtave weight)
![image info](./images/undirected_graph_weighted_edges.JPG)

```cpp
struct UndirectedGraphNode {
    int label;
    std::vector<std::pair<UndirectedGraphNode*, int>> neighbors;  // vertices and edge cost

    explicit UndirectedGraphNode(int x):label(x){};

    static void AddNeighbors(UndirectedGraphNode* A, UndirectedGraphNode* B, int const cost = 0){
        A->neighbors.emplace_back(B, cost);
        B->neighbors.emplace_back(A, cost);
    }
};
```

> data struct is defined in include/CommonStruct.h

##### 1. DFS Method
Find all possible paths from start to end and maintain the minimum cost.
```commandline
./GraphDFS
```

result:
```
cost from 0 to 5 is 13
cost from 0 to 4 is 11
cost from 0 to 1 is 1
cost from 0 to 2 is 9
cost from 0 to 3 is 6
```

##### 2. Dijkstra Method (original)
```commandline
./GraphDijkstra
```

result:
```
cost from 0 to 5 is: 13
cost from 0 to 4 is: 11
cost from 0 to 1 is: 1
cost from 0 to 2 is: 9
cost from 0 to 3 is: 6
```

Actually the original Dijkstra Method can be optimized by using minimum priority_queue.
Since in C++ `priority_queue` doesn't support index_ based query to update the cost `line21`, a customized **Indexed Min-Priority Queue** is needed in practice.
```cpp
1  function Dijkstra(Graph, source):
2      dist[source] ← 0                           // Initialization
3
4      create vertex set Q
5
6      for each vertex v in Graph:
7          if v ≠ source
8              dist[v] ← INFINITY                 // Unknown distance from source to v
9          prev[v] ← UNDEFINED                    // Predecessor of v
10
11         Q.add_with_priority(v, dist[v])
12
13
14     while Q is not empty:                      // The main loop
15         u ← Q.extract_min()                    // Remove and return best vertex
16         for each neighbor v of u:              // only v that are still in Q
17             alt ← dist[u] + length(u, v)
18             if alt < dist[v]
19                 dist[v] ← alt
20                 prev[v] ← u
21                 Q.decrease_priority(v, alt)    // <- Here we need to update cost in the priority_queue
22
23     return dist, prev
```
[Indexed Min-Priority Queue Time Complexity Reference](https://kartikkukreja.wordpress.com/2013/05/28/indexed-min-priority-queue-c-implementation/)

##### 3. Uniform Cost Search (UCS) Method
In common presentations of Dijkstra's algorithm, initially all nodes are entered into the priority queue. This is, however, not necessary: the algorithm can start with a priority queue that contains only one item, and insert new items as they are discovered (instead of doing a decrease-key, check whether the key is in the queue; if it is, decrease its key, otherwise insert it). This variant has the same worst-case bounds as the common variant, but maintains a smaller priority queue in practice, speeding up the queue operations.

Moreover, not inserting all nodes in a graph makes it possible to extend the algorithm to find the shortest path from a single source to the closest of a set of target nodes on infinite graphs or those too large to represent in memory. The resulting algorithm is called uniform-cost search (UCS) in the artificial intelligence literature.

> _Ref: Russell, Stuart; Norvig, Peter (2009) [1995]. Artificial Intelligence: A Modern Approach (3rd ed.). Prentice Hall. pp. 75, 84._

_This approach also requires decrease_priority functionality for `priority_queue`. However a walk around solution is instead of update the priority in the priority_queue, we can add a new `pair<cost, grapg_node>` which will consume extra space with same time complexity O(log(n))._

```commandline
./GraphUCS
```

```
cost from 0 to 5 is 13
cost from 0 to 4 is 11
cost from 0 to 1 is 1
cost from 0 to 2 is 9
cost from 0 to 3 is 6
```

#### Undirected Graph with Weighted Edge (With neigtave weight)
##### 1. DFS Method
Still works for this condition since it will find all possible paths.

