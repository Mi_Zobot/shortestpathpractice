#ifndef SHORTESTPATHPRACTICE_COMMONSTRUCT_H
#define SHORTESTPATHPRACTICE_COMMONSTRUCT_H

#include <climits>
#include <queue>
#include <vector>

struct UndirectedGraphNode {
    int label;
    std::vector<std::pair<UndirectedGraphNode*, int>> neighbors;  // vertices and edge cost

    explicit UndirectedGraphNode(int x):label(x){};

    static void AddNeighbors(UndirectedGraphNode* A, UndirectedGraphNode* B, int const cost = 0){
        A->neighbors.emplace_back(B, cost);
        B->neighbors.emplace_back(A, cost);
    }
};

#endif //SHORTESTPATHPRACTICE_COMMONSTRUCT_H
